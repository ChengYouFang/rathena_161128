//=============
//item_exchange By C.Y.Fang
//=============
prontera,62,63,3	script	研究所裝備	847,{
	mes "[研究所裝備]";
	mes "請問你要製作哪樣裝備?";
	next;
	.@select = select("守護巨人鋼盾:葛帔尼亞的古書(水):誓言之書(貳):救援斗篷:刺客的手銬:特務拳刃:斬首拳刃:火焰戰斧:急凍戰斧:鈣礦石長矛:守護巨人長矛:寒氣篇魔法書:元氣之光魔杖:綠色研究服:古代的金飾:綠光魔短劍:紅光魔短劍:嗜血十字錘:元戎弩:超異能十字弓:束縛之弓");
	mes "請輸入你要製作的數量";
	next;
	input .@num;
	if(preg_match("^[0-9]*[1-9][0-9]*$",.@num)==false){
		mes "請輸入製作正確的數量.";
		close;
	}else{
		switch(.@select){
			case 1:
				callsub M_1,2160,.@num*100,.@num*50,.@num*35,.@num;
			break;
			case 2:
				callsub M_1,2161,.@num*100,.@num*50,.@num*22,.@num;
			break;
			case 3:
				callsub M_1,2162,.@num*100,.@num*50,.@num*22,.@num;
			break;
			case 4:
				callsub M_1,2582,.@num*300,.@num*300,.@num*400,.@num;
			break;
			case 5:
				callsub M_1,2892,.@num*100,.@num*50,.@num*35,.@num;
			break;
			case 6:
				callsub M_1,1290,.@num*50,.@num*100,.@num*10,.@num;
			break;
			case 7:
				callsub M_1,1291,.@num*50,.@num*100,.@num*85,.@num;
			break;
			case 8:
				callsub M_1,1392,.@num*50,.@num*100,.@num*35,.@num;
			break;
			case 9:
				callsub M_1,1393,.@num*50,.@num*100,.@num*35,.@num;
			break;
			case 10:
				callsub M_1,1435,.@num*50,.@num*100,.@num*40,.@num;
			break;
			case 11:
				callsub M_1,1490,.@num*300,.@num*300,.@num*400,.@num;
			break;
			case 12:
				callsub M_1,1584,.@num*50,.@num*100,.@num*40,.@num;
			break;
			case 13:
				callsub M_1,1659,.@num*50,.@num*100,.@num*35,.@num;
			break;
			case 14:
				callsub M_1,15044,.@num*100,.@num*50,.@num*30,.@num;
			break;
			case 15:
				callsub M_1,18570,.@num*300,.@num*300,.@num*400,.@num;
			break;
			case 16:
				callsub M_1,13069,.@num*50,.@num*100,.@num*35,.@num;
			break;
			case 17:
				callsub M_1,13070,.@num*50,.@num*100,.@num*35,.@num;
			break;
			case 18:
				callsub M_1,16017,.@num*50,.@num*100,.@num*40,.@num;
			break;
			case 19:
				callsub M_1,18109,.@num*50,.@num*100,.@num*35,.@num;
			break;
			case 20:
				callsub M_1,18110,.@num*50,.@num*100,.@num*35,.@num;
			break;
			case 21:
				callsub M_1,18111,.@num*50,.@num*100,.@num*35,.@num;
			break;
		}
	}

M_1:
	setarray .@numbers[0], getarg(0),getarg(1), getarg(2), getarg(3),getarg(4);
	if(zeny<500000*.@numbers[4]){
		mes "身上的錢不足無法製作裝備，請再去多賺一些再回來吧!!";
		close;
	}else if(countitem(6469)<.@numbers[1] || countitem(6470)<.@numbers[2] || countitem(6471)<.@numbers[3]){
		mes "你的材料不足以製作，請準備好材料再來吧!!";
		close;
	}else if(checkweight(.@numbers[0],.@numbers[4]) < 1){
		mes "你的背包已滿，請將一些物品放入倉庫再來找我更換吧！！";
		close;
	}else{
		progressbar "FF00FF", @numbers[4];
		delitem 6469,.@numbers[1];
		delitem 6470,.@numbers[2];
		delitem 6471,.@numbers[3];
		getitem .@numbers[0],.@numbers[4];
		set Zeny,Zeny-(.@numbers[4]*500000);
		mes "製作成功";
		if(.@numbers[0]==2160)
			announce "["+strcharinfo(0)+"]  製作出了守護巨人鋼盾",0;
		else if(.@numbers[0]==2161)
			announce "["+strcharinfo(0)+"]  製作出了葛帔尼亞的古書(水)",0;
		else if(.@numbers[0]==2162)
			announce "["+strcharinfo(0)+"]  製作出了誓言之書(貳)",0;
		else if(.@numbers[0]==2582)
			announce "["+strcharinfo(0)+"]  製作出了救援斗篷",0;
		else if(.@numbers[0]==2892)
			announce "["+strcharinfo(0)+"]  製作出了刺客的手銬",0;
		else if(.@numbers[0]==1290)
			announce "["+strcharinfo(0)+"]  製作出了特務拳刃",0;
		else if(.@numbers[0]==1291)
			announce "["+strcharinfo(0)+"]  製作出了斬首拳刃",0;
		else if(.@numbers[0]==1392)
			announce "["+strcharinfo(0)+"]  製作出了火焰戰斧",0;
		else if(.@numbers[0]==1393)
			announce "["+strcharinfo(0)+"]  製作出了急凍戰斧",0;
		else if(.@numbers[0]==1435)
			announce "["+strcharinfo(0)+"]  製作出了鈣礦石長矛",0;
		else if(.@numbers[0]==1490)
			announce "["+strcharinfo(0)+"]  製作出了守護巨人長矛",0;
		else if(.@numbers[0]==1584)
			announce "["+strcharinfo(0)+"]  製作出了寒氣篇魔法書",0;
		else if(.@numbers[0]==1659)
			announce "["+strcharinfo(0)+"]  製作出了元氣之光魔杖",0;
		else if(.@numbers[0]==15044)
			announce "["+strcharinfo(0)+"]  製作出了綠色研究服",0;
		else if(.@numbers[0]==18570)
			announce "["+strcharinfo(0)+"]  製作出了古代的金飾",0;
		else if(.@numbers[0]==13069)
			announce "["+strcharinfo(0)+"]  製作出了綠光魔短劍",0;
		else if(.@numbers[0]==13070)
			announce "["+strcharinfo(0)+"]  製作出了紅光魔短劍",0;
		else if(.@numbers[0]==16017)
			announce "["+strcharinfo(0)+"]  製作出了嗜血十字錘",0;
		else if(.@numbers[0]==18109)
			announce "["+strcharinfo(0)+"]  製作出了元戎弩",0;
		else if(.@numbers[0]==18110)
			announce "["+strcharinfo(0)+"]  製作出了超異能十字弓",0;
		else if(.@numbers[0]==18111)
			announce "["+strcharinfo(0)+"]  製作出了束縛之弓",0;

		close;
	}

OnInit:
	waitingroom "研究所裝備",0;
	end;
}

prontera,59,63,3	script	神器	793,{
mes "[神器]";
mes "我們這邊能幫你製造以下這些神器";
next;
switch(select("^FF0000布林喜德^000000:^FF0000阿斯富力咖^000000:^FF0000雷神腰帶^000000:^FF0000女神頸鍊^000000:^FF0000史雷普尼爾之靴^000000:^FF0000雷神之錘^000000")){
	case 1:
		goto M_1;
	break;
	case 2:
		goto M_2;
	break;
	case 3:
		goto M_3;
	break;
	case 4:
		goto M_4;
	break;
	case 5:
		goto M_5;
	break;
	case 6:
		goto M_6;
	break;
}

OnInit:
        waitingroom "神器",0;
        end;
OverWeight:
		mes "[神器]";
		mes "不對哦，你已經超重了，等你把包包清空在來找我吧。";
		close;
M_1:
	mes "[^0000C6布林喜德^000000]所需材料為材料";
	mes "女神的眼淚*1 英雄的遺骸*1 巴基力的象徵*1";
	mes "布林喜德的鎧甲一角*1 安德巴里的戒指*1";
	mes "巴基力的鎧甲(1s)*1 鋁*20 巴哈拉的花*100 黃金*10";
	next;
	switch(select("製作.:算了吧，我改變心意了")){
		case 1:
			if(countitem(7830)<0 || countitem(7831)<0 || countitem(7832)<0 ||
	 			countitem(7833)<0 || countitem(7834)<0 || countitem(7510)<100 ||
	 			countitem(985)<20 || countitem(969)<10 || countitem(2357)<0){
	 				goto Error;
	 		}else{
				delitem 7830,1;
				delitem 7831,1;
		 		delitem 7832,1;
				delitem 7833,1;
				delitem 7834,1;
				delitem 7510,100;
				delitem 985,20;
				delitem 969,10;
				delitem 2357,1;
				getitem 2383,1;
				mes "恭喜你製作出了[^0000C6布林喜德^000000]";
				announce "["+strcharinfo(0)+"]  製作出了布林喜德",0;
		 		close;
	 		}
		break;
		case 2:
			goto Exit;
		break;
	}
M_2:
	mes "[^0000C6阿斯富力咖^000000]材料";
	mes "黑螢螢的星光*1 冰冷的月光*1 黃昏的光芒*1 黎明的清水*1";
	mes "柔軟的羽毛*100 鋁*20 瑪瑙*10 玫瑰石英*10 薄紗圍巾(1s)*1";
	next;
	switch(select("製作.:算了吧，我改變心意了")){
		case 1:
			if(countitem(7835)<0 || countitem(7836)<0 || countitem(7837)<0 ||
				countitem(7838)<0 || countitem(7863)<100 || countitem(985)<20 ||
				countitem(7291)<10 || countitem(7293)<10 || countitem(2513)<0){
					goto Error;
			}else{
				delitem 7835,1;
				delitem 7836,1;
				delitem 7837,1;
				delitem 7838,1;
				delitem 7863,100;
				delitem 985,20;
				delitem 7291,10;
				delitem 7293,10;
				getitem 2541,1;
				mes "恭喜你製作出了[^0000C6阿斯富力咖^000000]";
				announce "["+strcharinfo(0)+"]  製作出了阿斯富力咖",0;
				close;
			}
		break;
		case 2:
			goto Exit;
		break;
	}
M_3:
	mes "[^0000C6雷神腰帶^000000]材料";
	mes "魔法鐵鍊*1 黃金*20 藍寶石*10 神之金屬*10 皮帶*1";
	next;
	switch(select("製作.:算了吧，我改變心意了")){
		case 1:
			if(countitem(7058)<0 || countitem(969)<20 || countitem(726)<10 ||
				countitem(984)<10 || countitem(2627)<0){
					goto Error;
			}else{
				delitem 7058,1;
				delitem 969,20;
				delitem 726,10;
				delitem 984,10;
				delitem 2627,1;
				getitem 2629,1;
				mes "恭喜你製作出了[^0000C6雷神腰帶^000000]";
				announce "["+strcharinfo(0)+"]  製作出了雷神腰帶",0;
				close;
			}
		break;
		case 2:
			goto Exit;
		break;
	}
M_4:
	mes "[^0000C6女神頸鍊^000000]材料";
	mes "菲力亞寶石*4 銀飾品*4 雪的結晶*3 安靜的水波*3 空氣雕像*3";
	mes "藍寶石*2 珍珠*3 白寶石*10 詛咒紅寶石*5 黃金*20 體力項鍊(0s)*1";
	next;
	switch(select("製作.:算了吧，我改變心意了")){
		case 1:
			if(countitem(7073)<4 || countitem(7077)<4 || countitem(7088)<3 ||
				countitem(7090)<3 || countitem(7092)<3 || countitem(726)<2 ||
				countitem(722)<3 || countitem(727)<10 || countitem(724)<5 ||
				countitem(969)<20 || countitem(2603)<1){
					goto Error;
			}else{
				delitem 7073,4;
				delitem 7077,4;
				delitem 7088,3;
				delitem 7090,3;
				delitem 7092,3;
				delitem 726,2;
				delitem 722,3;
				delitem 727,10;
				delitem 724,5;
				delitem 969,20;
				delitem 2603,1;
				getitem 2630,1;
				mes "恭喜你製作出了[^0000C6女神頸鍊^000000]";
				announce "["+strcharinfo(0)+"]  製作出了女神頸鍊",0;
				close;
			}
		break;
		case 2:
			goto Exit;
		break;
	}
M_5:
	mes "[^0000C6史雷普尼爾之靴^000000]材料";
	mes "未知的鋸齒輪子*3 天使翅膀羽毛*5 魚的靈魂*3 太陽神的象徵*4";
	mes "靈魂的呼吸聲音*3 黃金*20 鋁*10  戰士長靴(1S)*1";
	switch(select("製作.:算了吧，我改變心意了")){
		case 1:
			if(countitem(7076)<3 || countitem(7079)<5 || countitem(7083)<3 ||
				countitem(7087)<3 || countitem(7086)<4 || countitem(985)<10 ||
				countitem(969)<20 || countitem(2406)<0){
					goto Error;
			}else{
				delitem 7076,3;
				delitem 7079,5;
				delitem 7083,3;
				delitem 7087,3;
				delitem 7086,4;
				delitem 985,10;
				delitem 969,20;
				delitem 2406,1;
				getitem 2410,1;
				mes "恭喜你製作出了[^0000C6史雷普尼爾之靴^000000]";
				announce "["+strcharinfo(0)+"]  製作出了史雷普尼爾之靴",0;
				close;
			}
		break;
		case 2:
			goto Exit;
		break;
	}
M_6:
	mes "[^0000C6雷神之錘^000000]材料";
	mes "拖爾的鐵手套*2 拷問器具*4 巴吉利的憤怒*5 暴風雨的徵兆*5";
	mes "海浪雕像*5 神之金屬*20 鋁*5 黃金*40 昏迷之錘(0s)*1";
	switch(select("製作.:算了吧，我改變心意了")){
		case 1:
			if(countitem(7074)<2 || countitem(7075)<4 || countitem(7078)<5 ||
				countitem(7089)<5 || countitem(7091)<5 || countitem(984)<20 ||
				countitem(985)<5 || countitem(969)<40 || countitem(1522)<0){
					goto Error;
			}else{
				delitem 7074,2;
				delitem 7075,4;
				delitem 7078,5;
				delitem 7089,5;
				delitem 7091,5;
				delitem 984,20;
				delitem 985,5;
				delitem 969,40;
				delitem 1522,1;
				getitem 1530,1;
				mes "恭喜你製作出了[^0000C6雷神之錘^000000]";
				announce "["+strcharinfo(0)+"]  製作出了雷神之錘",0;
				close;
			}
		break;
		case 2:
			goto Exit;
		break;
	}
Error:
	mes "[神器]";
	mes "等你收集好材料在來找我吧!!";
	close;
Exit:
	mes "[神器]";
	mes "等你想好在來找我吧!!";
	close;
}

prontera,146,98,3	script	小神鋁換大神鋁	63,{
	mes "請問你要將神之金屬原石換成神之金屬呢，還是將鋁原石換成鋁？";
	next;
	switch(select("神之金屬:鋁")){
		case 1:
			@num = countitem(756)/5;
			if(@num<=0){
				mes "真可惜，你的數量不足以更換神之金屬.";
			}else if(checkweight(984,@num) < 1){
				goto OverWeight;
			}else{
				delitem 756,@num*5;
				getitem 984,@num;
			}
		break;
		case 2:
			@num = countitem(757)/5;
			if(@num<=0){
				mes "真可惜，你的數量不足以更換鋁.";
			}else if(checkweight(985,@num) < 1){
				goto OverWeight;
			}else{
				delitem 757,@num*5;
				getitem 985,@num;
			}
		break;
	}
	close;

OverWeight:
		mes "[小神鋁換大神鋁]";
		mes "你的背包已滿，請將一些物品放入倉庫再來找我更換吧！！";
		close;
}

